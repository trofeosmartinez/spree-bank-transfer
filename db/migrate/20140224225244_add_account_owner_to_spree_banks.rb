class AddAccountOwnerToSpreeBanks < ActiveRecord::Migration
  def change
    add_column :spree_banks, :account_owner, :string
  end
end
