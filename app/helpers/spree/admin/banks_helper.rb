module Spree
  module Admin
    module BanksHelper

      def bank_status(bank)
        bank.active? ? 'Activa' : 'Inactiva'
      end

      def action_to_toggle_bank_status(bank) 
        bank.active? ? 'Desactivar' : 'Activar'
      end

    end
  end
end